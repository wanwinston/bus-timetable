﻿using BusTimetable.Core.Domain;

namespace BusTimetable.Core.Repository
{
    public interface IRepository<T> 
        where T : BaseEntity
    {
        T Get(object id);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        void Delete(int id);
    }
}
