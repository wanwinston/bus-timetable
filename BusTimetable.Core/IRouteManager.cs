﻿using BusTimetable.Core.Domain;
using BusTimetable.Core.Dtos;
using System.Collections.Generic;

namespace BusTimetable.Core
{
    public interface IRouteManager
    {
        /// <summary>
        /// Gets a route by route id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Route GetRouteById(int id);
        /// <summary>
        /// Gets the arrival times of a station
        /// </summary>
        /// <param name="id">The Station Id</param>
        /// <param name="noOfArrivals">Number of Arrivals to retrieve</param>
        /// <returns></returns>
        List<RouteArrivalTimeDto> GetArrivalTimesByStationId(int id, int noOfArrivals);
    }
}
