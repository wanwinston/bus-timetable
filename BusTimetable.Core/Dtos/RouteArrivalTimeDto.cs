﻿using System.Collections.Generic;

namespace BusTimetable.Core.Dtos
{
    public class RouteArrivalTimeDto
    {
        public int StationId { get; set; }
        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public List<BusArrivalTimeDto> ArrivalTimes { get; set; }
    }
}
