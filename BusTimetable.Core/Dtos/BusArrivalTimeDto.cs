﻿using System;

namespace BusTimetable.Core.Dtos
{
    public class BusArrivalTimeDto
    {
        public DateTime ArrivalTime { get; set; }
        public int BusId { get; set; }
    }
}
