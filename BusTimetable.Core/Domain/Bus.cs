﻿using System;

namespace BusTimetable.Core.Domain
{
    public class Bus : BaseEntity
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public int RouteId { get; set; }
        public virtual Route Route { get; set; }
    }
}
