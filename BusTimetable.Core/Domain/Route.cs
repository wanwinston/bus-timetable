﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusTimetable.Core.Domain
{
    public class Route : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
