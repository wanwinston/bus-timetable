﻿namespace BusTimetable.Core.Domain
{
    public class Station : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
