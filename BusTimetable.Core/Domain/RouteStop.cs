﻿using System;

namespace BusTimetable.Core.Domain
{
    public class RouteStop : BaseEntity
    {
        public int RouteId { get; set; }
        public int StationId { get; set; }
        public int Order { get; set; }
        public TimeSpan TimeFromLastStop { get; set; }

        public virtual Route Route { get; set; }
        public virtual Station Station { get; set; }
    }
}
