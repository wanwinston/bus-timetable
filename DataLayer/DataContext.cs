﻿using BusTimetable.Core.Domain;
using System;
using System.Collections.Generic;

namespace BusTimetable.DataLayer
{
    public static class DataContext
    {
        // initializes with fake data
        public static void Initialize()
        {
            Stations = new List<Station>();
            Routes = new List<Route>();

            // add 10 stations
            for (int i = 1; i <= 10; i++)
            {
                Stations.Add(new Station
                {
                    Id = i,
                    Name = string.Format("Stop {0}", i)
                });
            }

            // add 3 routes
            for (int i = 1; i <= 3; i++)
            {
                Routes.Add(new Route
                {
                    Id = i,
                    Name = string.Format("Route {0}", i)
                });
            }

            // all routes stop at each station
            RouteStops = new List<RouteStop>();
            for (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    var routeStop = new RouteStop
                    {
                        RouteId = i,
                        StationId = j,
                        Order = j,
                        TimeFromLastStop = j == 1 ? TimeSpan.FromMinutes(0) : TimeSpan.FromMinutes(2),
                        Route = Routes[i - 1],
                        Station = Stations[j - 1]
                    };

                    RouteStops.Add(routeStop);
                }
            }

            // line up the buses. Assume we have infinite buses
            Buses = new List<Bus>();
            int id = 0;
            for (int i = 1; i <= 3; i++)
            {
                var startTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, (i-1) * 2, 0);
                var endTime = startTime.AddDays(1);

                while (startTime < endTime)
                {
                    startTime = startTime.AddMinutes(15);
                    Buses.Add(new Bus
                    {
                        Id = id++,
                        StartTime = startTime,
                        RouteId = i,
                        Route = Routes[i - 1]
                    });
                }
            }
        }


        public static List<Bus> Buses { get; set; }
        public static List<Route> Routes { get; set; }
        public static List<RouteStop> RouteStops { get; set; }
        public static List<Station> Stations { get; set; }
    }
}
