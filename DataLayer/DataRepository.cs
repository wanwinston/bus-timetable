﻿using BusTimetable.Core.Domain;
using BusTimetable.Core.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusTimetable.DataLayer
{
    // Tried to make a fake generic repository pattern, but can't without a db context

    public class DataRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        //internal readonly DataContext _dataContext;
        //internal readonly IEnumerable<T> _entities;

        //public DataRepository(DataContext context)
        //{
        //    _dataContext = context;
        //}

        public void Add(T entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public T Get(object id)
        {
            throw new NotImplementedException();
        }

        public void Update(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
