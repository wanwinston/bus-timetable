﻿using BusTimetable.Core;
using BusTimetable.Core.Domain;
using BusTimetable.Core.Dtos;
using BusTimetable.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusTimetable.BusinessLayer
{
    public class RouteManager : IRouteManager
    {
        public Route GetRouteById(int id)
        {
            var test = GetArrivalTimesByStationId(2, 2);
            return DataContext.Routes.FirstOrDefault(x => x.Id == id);
        }

        public List<RouteArrivalTimeDto> GetArrivalTimesByStationId(int id, int noOfArrivals)
        {
            var routeArrivalTimes = new List<RouteArrivalTimeDto>();

            // get all the route stops for this station
            var routeStops = DataContext.RouteStops.Where(x => x.StationId == id);

            foreach (var routeStop in routeStops)
            {
                // query to calculate arrival time
                var query = from buses in DataContext.Buses
                            join rs in DataContext.RouteStops
                                on buses.RouteId equals rs.RouteId
                            let arrivalTime = buses.StartTime.AddMinutes(DataContext.RouteStops.Where(x => x.RouteId == routeStop.RouteId && x.Order <= rs.Order).Sum(x => x.TimeFromLastStop.Minutes))
                            where rs.RouteId == routeStop.RouteId && rs.StationId == id && arrivalTime > DateTime.Now
                            orderby arrivalTime
                            select new BusArrivalTimeDto
                            {
                                BusId = buses.Id,
                                ArrivalTime = arrivalTime,
                            };

                // construct the Dto
                routeArrivalTimes.Add(new RouteArrivalTimeDto
                {
                    StationId = id,
                    RouteId = routeStop.RouteId,
                    RouteName = routeStop.Route.Name,
                    ArrivalTimes = query.Take(noOfArrivals).ToList()
                });
            }

            return routeArrivalTimes;
        }
    }
}
