﻿using AutoMapper;
using BusTimetable.Core.Dtos;
using BusTimetable.Models;
using System;
using System.Linq;

namespace BusTimetable.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<RouteArrivalTimeDto, RouteArrivalTimeModel>()
                   .ForMember(d => d.ArrivalMinutes, opt => opt.MapFrom(src => src.ArrivalTimes.Select(x => x.ArrivalTime.Subtract(DateTime.Now).Minutes)))
                   .ForMember(d => d.Key, opt => opt.MapFrom(src => string.Format("{0}{1}", src.StationId, src.RouteId)));
            });
        }
    }
}
