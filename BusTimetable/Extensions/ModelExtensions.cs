﻿using AutoMapper;
using BusTimetable.Core.Dtos;
using BusTimetable.Models;
using System.Collections.Generic;

namespace BusTimetable.Extensions
{
    public static class ModelExtensions
    {
        public static List<RouteArrivalTimeModel> ToModel(this List<RouteArrivalTimeDto> source)
        {
            return Mapper.Map<List<RouteArrivalTimeDto>, List<RouteArrivalTimeModel>>(source);
        }
    }
}
