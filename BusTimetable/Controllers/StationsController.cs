﻿using BusTimetable.Core;
using BusTimetable.Core.Domain;
using BusTimetable.Extensions;
using BusTimetable.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BusTimetable.Controllers
{
    [Route("api/[controller]")]
    public class StationsController : Controller
    {
        public IRouteManager _routeManager;

        public StationsController(IRouteManager routeManager)
        {
            _routeManager = routeManager;
        }

        // GET api/values
        [HttpGet]
        [Route("routearrivaltimes/{id}")]
        public List<RouteArrivalTimeModel> GetRouteArrivalTimes(int id, int count = 2)
        {
            var route = _routeManager.GetArrivalTimesByStationId(id, count);
            return route.ToModel();
        }
    }
}
