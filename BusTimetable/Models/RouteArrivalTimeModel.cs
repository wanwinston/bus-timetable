﻿using System.Collections.Generic;

namespace BusTimetable.Models
{
    public class RouteArrivalTimeModel
    {
        public int RouteId { get; set; }
        public int StationId { get; set; }
        public string Key { get; set; }
        public string RouteName { get; set; }
        public List<int> ArrivalMinutes { get; set; }
    }
}
