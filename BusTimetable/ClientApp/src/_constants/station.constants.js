﻿export const stationConstants = {
    ARRIVAL_TIMES_REQUEST: 'STATION_ARRIVAL_TIMES_REQUEST',
    ARRIVAL_TIMES_SUCCESS: 'STATION_ARRIVAL_TIMES_SUCCESS',
    ARRIVAL_TIMES_FAILURE: 'STATION_ARRIVAL_TIMES_FAILURE'
}