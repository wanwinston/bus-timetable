﻿import { combineReducers } from 'redux';
import { station } from './station.reducer'

const rootReducer = combineReducers({
    1: station('1'),
    2: station('2')
});

export default rootReducer;