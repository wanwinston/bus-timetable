﻿import { stationConstants } from '../_constants/station.constants';

export const station = (stationId) => (state = {}, action) => {
    switch (action.type) {
        case `${stationId}/${stationConstants.ARRIVAL_TIMES_REQUEST}`:
            return {
                loading: true
            };
        case `${stationId}/${stationConstants.ARRIVAL_TIMES_SUCCESS}`:
            return {
                station : {
                    items: action.arrivalTimes,
                    stationId: action.id
                }
            };
        case `${stationId}/${stationConstants.ARRIVAL_TIMES_FAILURE}`:
            return {
                items: action.error
            };
        default:
            return state;
    }
}