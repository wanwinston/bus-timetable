﻿import { stationService } from '../_services/station.service';
import { stationConstants } from '../_constants/station.constants';

export const stationActions = {
    getArrivalTimesById
}

function getArrivalTimesById(id, count) {
    return dispatch => {
        dispatch(request(id));
        
        stationService.getArrivalTimesById(id, count)
        .then(
            arrivalTimes => dispatch(success(arrivalTimes, id)),
            error => dispatch(failure(error))
            );
    }

    function request(id) { return { type: `${id}/${stationConstants.ARRIVAL_TIMES_REQUEST}`, id } }
    function success(arrivalTimes, id) { return { type: `${id}/${stationConstants.ARRIVAL_TIMES_SUCCESS}`, arrivalTimes, id } }
    function failure(error) { return { type: `${id}/${stationConstants.ARRIVAL_TIMES_FAILURE}`, error } }
}