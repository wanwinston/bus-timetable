﻿import { config } from '../_helpers/config';
require('es6-promise').polyfill();
import 'whatwg-fetch'

export const stationService = {
    getArrivalTimesById
}

function getArrivalTimesById(id, count) {
    const requestOptions = {
        method: 'GET'
    }

    return fetch(config.apiUrl + '/stations/routearrivaltimes/' + id + '?count=' + count, requestOptions).then(handleResponse, handleError);
}

function handleResponse(response) {
    return new Promise((resolve, reject) => {
        if (response.ok) {
            // return json if it was returned in the response
            var contentType = response.headers.get("content-type");
            if (contentType && contentType.includes("application/json")) {
                response.json().then(json => resolve(json));
            } else {
                resolve();
            }
        } else {
            // return error message from response body
            response.text().then(text => reject(text));
        }
    });
}

function handleError(error) {
    return Promise.reject(error && error.message);
}