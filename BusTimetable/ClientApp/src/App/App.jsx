﻿import React from 'react';
import { ArrivalTimes } from '../ArrivalTimes/ArrivalTimes';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        };
    }

    componentDidMount() {
        setInterval(() => {
            this.setState({
                date: new Date()
            })
        }, 60000)
    }

    render() {
        return (
            <div>
                <h3 className="align-center time-padding">{this.state.date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}</h3>
                <ArrivalTimes stationId={1} timeCount="2" />
                <ArrivalTimes stationId={2} timeCount="2" />
            </div>
        );
    }
}

export default App;