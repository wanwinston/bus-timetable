﻿import React from 'react';
import ReactDom from 'react-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { stationActions } from '../_actions/station.actions'

class ArrivalTimes extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(stationActions.getArrivalTimesById(this.props.stationId, this.props.timeCount));
        setInterval(() => this.props.dispatch(stationActions.getArrivalTimesById(this.props.stationId, this.props.timeCount)), 60000);
    }

    render() {
        const { station } = this.props;
        return (
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title align-center">Station {this.props.stationId} </h4>
                    {!!station && !!station.items &&
                        <div className="align-center">
                        {station.items.map((routeTime, index) =>
                            <div key={routeTime.key} className="card-text">
                                {routeTime.routeName} in {routeTime.arrivalMinutes[0]} mins and {routeTime.arrivalMinutes[1]} mins
                            </div>
                        )}
                        </div>
                    }
                </div>
            </div>
        )
    }
}

ArrivalTimes.propTypes = {
    stationId: PropTypes.number
}

function mapStateToProps(state, ownProps) {
    let myState = state[ownProps.stationId];
    const { station } = myState;
    return {
        station
    };
}

const connectedArrivalTimes = connect(mapStateToProps)(ArrivalTimes);
export { connectedArrivalTimes as ArrivalTimes };