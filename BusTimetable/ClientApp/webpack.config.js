﻿var path = require('path');

const config = {
    entry: './src/index.jsx',
    output: {
        path: path.resolve('dist'),
        filename: '../../wwwroot/bundle.js',
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-3']
                }
            }
        ]
    },
    plugins: [

    ]
};

module.exports = config;