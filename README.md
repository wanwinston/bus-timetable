# README #

* Bus Timetable Web Application
* 1.0
* https://bitbucket.org/wanwinston/bus-timetable/

# Instructions #
* Requirements: Visual Studio 2017

* Steps:
	1. In the root directory, open **BusTimeTable.sln**
	2. In the 'Start Debugging' dropdown (green play button with IIS Express selected)
		* choose **BusTimetable**
		* choose any browser, but my preference is Chrome
	3. Be sure to set the **BusTimetable* web app as the start up project or select it in the solution explorer
	4. Click the green play button to start the app
	
* Database
	* There is no database. I inserted all the data need in memory for the sake of test.
	* BusTimetable.DataLayer mocks the DB Access layer. I began the repository pattern, but without a DbContext, it could not be completed.
		* BusTimeTable.DataContext mocks an ORM
	* BusTimetable.Core.Domain contains all the domain objects that would've mapped to the DB
	* One day of information is loaded into the context
	
* React
	* All the react files are bundled into bundle.js using webpack and put in the wwwroot folder.
	* To make changes, run **webpack** in the folder:  **/BusTimetable/ClientApp**

