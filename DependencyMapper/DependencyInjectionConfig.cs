﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using BusTimetable.BusinessLayer;
using BusTimetable.Core;
using BusTimetable.DataLayer;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DependencyMapper
{
    public class DependencyInjectionConfig
    {
        public static IServiceProvider Register(IContainer container, IServiceCollection services)
        {
            DataContext.Initialize();

            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterType<RouteManager>().As<IRouteManager>().InstancePerLifetimeScope();

            container = builder.Build();

            return new AutofacServiceProvider(container);
        }
    }
}
